import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ICandidate} from "../types/types";

@Injectable()
export class UserService {
  public candidate: ICandidate | null = null;
  public isAdmin = false;

  constructor() {
  }
}
