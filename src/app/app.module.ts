import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconRegistry, MatIconModule} from '@angular/material/icon';
import {QuestionnaireComponent} from './questionnaire/questionnaire.component';
import {FetcherService} from '../services/fetcher.service';
import {CandidatesComponent} from './candidates/candidates.component';
import {NineBoxComponent} from './nine-box/nine-box.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {HttpClientModule} from '@angular/common/http';
import {AdminPageComponent} from './admin-page/admin-page.component';
import {FormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTableModule} from '@angular/material/table';
import * as CanvasJSAngularChart from '../assets/canvasjs.angular.component';
import { LoginComponent } from './login/login.component';
import {UserService} from "../services/user.service";
import {MatSelectModule} from "@angular/material/select";
import { HomeComponent } from './home/home.component';
const CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;

@NgModule({
  declarations: [
    AppComponent,
    QuestionnaireComponent,
    CandidatesComponent,
    NineBoxComponent,
    AdminPageComponent,
    CanvasJSChart,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    MatInputModule,
    FormsModule,
    MatCheckboxModule,
    MatTableModule,
    MatSelectModule
  ],
  providers: [FetcherService, UserService],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private iconRegister: MatIconRegistry, private sanitizer: DomSanitizer) {
    this.registerIcon('9box');
    this.registerIcon('candidates');
    this.registerIcon('quiz');
    this.registerIcon('box');
    this.registerIcon('admin');
    this.registerIcon('login');

  }

  private registerIcon(iconName: string): void {
    this.iconRegister.addSvgIcon(iconName, this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/' + iconName + '.svg'));
  }
}
