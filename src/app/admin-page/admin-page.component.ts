import {Component, OnInit} from '@angular/core';
import {FetcherService} from '../../services/fetcher.service';
import * as XLSX from 'xlsx';
import {goToHome, ICandidate, ICycle, initCandidate, initSotzyometry} from '../../types/types';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.less']
})
export class AdminPageComponent implements OnInit {
  private candidates: any[] = [];
  private sotzyometryResults: any[] = [];
  public cycles: ICycle[] = [];

  public candidate: ICandidate = initCandidate;
  public sotzyometry = initSotzyometry;
  public cycleName = '';


  public showAddCandidateDivv = false;
  public showAddSotzyometryDivv = false;
  public showCycleDivv = false;

  constructor(private fetcher: FetcherService, private user: UserService) {
  }

  async ngOnInit(): Promise<void> {
    if (!this.user.isAdmin) {
      goToHome();
    }
    this.getCycles();
  }

  public async readExcel(event: any): Promise<void> {
    const file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.readAsBinaryString(file);
    fileReader.onload = async (e) => {
      const workBook = XLSX.read(fileReader.result, {type: 'binary'});
      const sheetNames = workBook.SheetNames;
      this.candidates = XLSX.utils.sheet_to_json(workBook.Sheets[sheetNames[0]]);
      try {
        await this.fetcher.post('uploadCandidates', this.candidates);
        alert('העלאת מועמדים בוצעה בהצלחה :)');
      } catch (e) {
        alert('העלאת מועמדים נכשלה נסה שוב');
      }
    };
  }

  public async readExcel2(event: any): Promise<void> {
    const file = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.readAsBinaryString(file);
    fileReader.onload = async (e) => {
      const workBook = XLSX.read(fileReader.result, {type: 'binary'});
      const sheetNames = workBook.SheetNames;
      this.sotzyometryResults = XLSX.utils.sheet_to_json(workBook.Sheets[sheetNames[0]]);
      try {
        await this.fetcher.post('uploadSotzyometryResults', this.sotzyometryResults);
        alert('העלאת תוצאות סוציומטרי בוצעה בהצלחה :)');
      } catch (e) {
        alert('העלאת תוצאות סוציומטרי נכשלה נסה שוב');
      }
    };
  }

  public clickButton(id: string): void {
    const button = document.getElementById(id);
    if (button) {
      button.click();
    }
  }

  public async deleteCandidates(): Promise<void> {
    try {
      await this.fetcher.post('deleteAllCandidates', null);
      alert('רשימת המועמדים נמחקה בהצלחה');
    }
    catch (e) {
      alert('שגיאה נסה שוב מאוחר יותר');
    }
  }

  public showAddCandidateDiv(): void {
    this.showAddCandidateDivv = true;
  }

  public hideAddCandidateDiv(): void {
    this.showAddCandidateDivv = false;
  }

  public showAddSotzyometryDiv(): void {
    this.showAddSotzyometryDivv = true;
  }

  public hideAddSotzyometryDiv(): void {
    this.showAddSotzyometryDivv = false;
  }

  public showCycleDiv(): void {
    this.showCycleDivv = true;
  }

  public hideCycleDiv(): void {
    this.showCycleDivv = false;
  }

  public async addCandidate(): Promise<void> {
    try {
      await this.fetcher.post('addCandidate', this.candidate);
      this.candidate = initCandidate;
      this.showAddCandidateDivv = false;
      alert('המועמד התווסף בהצלחה');
    }
    catch (e) {
      alert('שגיאה נסה שוב מאוחר יותר');
    }
  }

  public async addSotzyometry(): Promise<void> {
    try {
      await this.fetcher.post('addSotzyometry', this.sotzyometry);
      this.sotzyometry = initSotzyometry;
      this.showAddSotzyometryDivv = false;
      alert('הסוציומטרי התווסף בהצלחה');
    }
    catch (e) {
      alert('שגיאה נסה שוב מאוחר יותר');
    }
  }

  public async addCycle(): Promise<void> {
    try {
      await this.fetcher.post('createCycle', {name: this.cycleName});
      this.cycleName = '';
      this.showCycleDivv = false;
      alert('נפתח מחזור חדש :)');
    }
    catch (e) {
      alert('שגיאה נסה שוב מאוחר יותר');
    }
    this.getCycles();
  }

  public async getCycles(): Promise<void> {
    try {
      this.cycles = await this.fetcher.get('getCycles');
    } catch (e) {
      alert('שגיאה בהבאת מידע מהשרת');
    }
  }

  public canAddCycle(): boolean {
    return this.cycleName.length > 0;
  }

  public canAddCandidate(): boolean {
    return this.candidate.MA !== null &&
      this.candidate.name.length > 0 &&
      this.candidate.tafkid.length > 0 &&
      this.candidate.age !== null &&
      this.candidate.vetek !== null &&
      this.candidate.gender.length > 0 &&
      this.candidate.darga.length > 0 &&
      this.candidate.slavesSum !== null &&
      this.candidate.merkaz.length > 0 &&
      this.candidate.anaf.length > 0 &&
      this.candidate.mador.length > 0;
  }

  public canAddSotzyometry(): boolean {
    return this.sotzyometry.MA !== null &&
      this.sotzyometry.bitzuaScore !== null &&
      this.sotzyometry.potentialScore !== null &&
      this.sotzyometry.cycleId.length > 0;
  }
}
