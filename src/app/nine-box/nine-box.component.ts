import {Component, OnInit, ViewChild} from '@angular/core';
import {FetcherService} from '../../services/fetcher.service';
import {goToHome, ICandidate, ICycle, INineBoxData, initCandidate, NineBoxDataDict} from '../../types/types';
import {MatTable} from '@angular/material/table';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-nine-box',
  templateUrl: './nine-box.component.html',
  styleUrls: ['./nine-box.component.less']
})
export class NineBoxComponent implements OnInit {
  @ViewChild(MatTable) table: MatTable<ICandidate>;
  public displayedColumns: string[] = Object.keys(initCandidate);
  public cycles: ICycle[] = [];
  public candidates: ICandidate[] = [];
  public filteredCandidates: ICandidate[] = [];
  public chosenCandidates: ICandidate[] = [];
  public chosenCycles: ICycle[] = [];
  public nineBoxDataDict: NineBoxDataDict;
  public nineBoxDataDictValues: Array<INineBoxData[]>;
  public chartData: any = null;
  public invalidData: any[] = [];
  public filterText = '';
  public displayText = 'טוען...';
  public isLoading = false;

  constructor(private fetcher: FetcherService, private user: UserService) {
  }

  async ngOnInit(): Promise<void> {
    if (!this.user.isAdmin) {
      goToHome();
    }
    try {
      this.candidates = await this.fetcher.get('getPeople');
      this.cycles = await this.fetcher.get('getCycles');
      this.filteredCandidates = this.candidates;
      if (this.candidates.length === 0) {
        this.displayText = 'רשימת המועמדים ריקה';
      }
    } catch (e) {
      this.displayText = 'שגיאה בשליפת רשימת המועמדים מהשרת';
    }
  }

  public clickRow(row: ICandidate): void {
    const index = this.chosenCandidates.findIndex((candidate: ICandidate) => candidate.MA === row.MA);
    if (index !== -1) {
      this.chosenCandidates.splice(index, 1);
    } else {
      if (row.MA) {
        this.chosenCandidates.push(row);
      }
    }
  }

  public filter(): void {
    this.filteredCandidates = this.candidates.filter((candidate: ICandidate) => {
      let isFound = false;
      this.displayedColumns.forEach(key => {
        if (String((candidate as any)[key]).includes(this.filterText)) {
          isFound = true;
        }
      });
      return isFound;
    });
    this.table.renderRows();
  }

  public isRowSelected(row: ICandidate): boolean {
    return this.chosenCandidates.some((candidate: ICandidate) => candidate.MA === row.MA);
  }

  public isAnyChosen(): boolean {
    return this.chosenCandidates.length > 0;
  }

  public canCalc9box(): boolean {
    return this.isAnyChosen() && this.chosenCycles.length > 0;
  }

  public cleanChosen(): void {
    this.chosenCandidates = [];
  }

  public chooseAll(): void {
    this.filteredCandidates.forEach((candidate: ICandidate) => {
      if (!this.isRowSelected(candidate) && candidate.MA) {
        this.chosenCandidates.push(candidate);
      }
    });
  }

  public async calc9box(): Promise<void> {
    try {
      this.isLoading = true;
      this.displayText = 'מחשב 9BOX...';
      const result = await this.fetcher.post('getNineBoxResults', {
        chosenCandidates: this.chosenCandidates,
        chosenCycles: this.chosenCycles
      });
      this.chartData = result.chartData;
      this.nineBoxDataDict = result.nineBoxDataDict;
      this.nineBoxDataDictValues = Object.values(this.nineBoxDataDict)
      this.invalidData = result.invalidData;
      this.isLoading = false;
    } catch (e) {
      this.displayText = 'שגיאה בחישוב 9BOX...';
      alert('שגיאה בחישוב 9BOX');
    }
  }
}
