import {Component, OnInit} from '@angular/core';
import {FetcherService} from '../../services/fetcher.service';
import {ICandidate, IQuiz} from '../../types/types';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.less']
})
export class QuestionnaireComponent implements OnInit {
  public candidate: ICandidate | null = null;
  public questionnaireAnswers: IQuiz = {
    commanderMA: null,
    isDirectCommander: false,
    MA: 0,
    bitzua1: 3,
    bitzua2: 3,
    bitzua3: 3,
    potential1: 3,
    potential2: 3,
    potential3: 3
  };

  constructor(private fetcher: FetcherService, private user: UserService) {
    this.candidate = user.candidate;
    if (this.candidate) {
      this.questionnaireAnswers.MA = this.candidate.MA;
    }
  }

  ngOnInit(): void {
  }

  public increaseShit(answer: string): void {
    if ((this.questionnaireAnswers as any)[answer] < 7) {
      (this.questionnaireAnswers as any)[answer] = (this.questionnaireAnswers as any)[answer] + 1;
    }
  }

  public decreaseShit(answer: string): void {
    if ((this.questionnaireAnswers as any)[answer] > 1) {
      (this.questionnaireAnswers as any)[answer] = (this.questionnaireAnswers as any)[answer] - 1;
    }
  }

  public canSave(): boolean {
    const areAllResultsValid = this.questionnaireAnswers.bitzua1 <= 7 && this.questionnaireAnswers.bitzua1 > 0 &&
      this.questionnaireAnswers.bitzua2 <= 7 && this.questionnaireAnswers.bitzua2 > 0 &&
      this.questionnaireAnswers.bitzua3 <= 7 && this.questionnaireAnswers.bitzua3 > 0 &&
      this.questionnaireAnswers.potential1 <= 7 && this.questionnaireAnswers.potential1 > 0 &&
      this.questionnaireAnswers.potential2 <= 7 && this.questionnaireAnswers.potential2 > 0 &&
      this.questionnaireAnswers.potential3 <= 7 && this.questionnaireAnswers.potential3 > 0;
    return areAllResultsValid &&
      this.questionnaireAnswers.commanderMA !== null &&
      String(this.questionnaireAnswers.commanderMA).length === 7;
  }

  public async saveQuestionnaire(): Promise<void> {
    try {
      await this.fetcher.post('saveQuiz', this.questionnaireAnswers);
      this.user.candidate = null;
      this.candidate = null;
      alert('שאלון נשמר בהצלחה');
    } catch (e) {
      if (e.status === 505) {
        alert('שגיאה! כבר מילאת שאלון על המועמד הנ"ל');
        this.user.candidate = null;
        this.candidate = null;
      } else {
        alert('שגיאה! אנא נסה שוב מאוחר יותר');
      }
    }
  }
}
