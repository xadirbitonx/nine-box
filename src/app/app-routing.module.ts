import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {QuestionnaireComponent} from './questionnaire/questionnaire.component';
import {CandidatesComponent} from './candidates/candidates.component';
import {NineBoxComponent} from './nine-box/nine-box.component';
import {AdminPageComponent} from './admin-page/admin-page.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';

const routes: any = [
  {path: 'questionnaire', component: QuestionnaireComponent},
  {path: 'candidates', component: CandidatesComponent},
  {path: '9box', component: NineBoxComponent},
  {path: 'admin', component: AdminPageComponent},
  {path: 'login', component: LoginComponent},
  {path: '', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
