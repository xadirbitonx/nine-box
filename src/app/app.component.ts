import {Component, OnInit} from '@angular/core';
import {FetcherService} from '../services/fetcher.service';
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  constructor(private fetcher: FetcherService, private user: UserService) {
  }

  ngOnInit(): void {

  }

  public isAdmin(): boolean {
    return this.user.isAdmin;
  }
}
