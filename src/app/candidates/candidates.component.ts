import {Component, OnInit} from '@angular/core';
import {FetcherService} from '../../services/fetcher.service';
import {ICandidate, initCandidate} from '../../types/types';
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.less']
})
export class CandidatesComponent implements OnInit {
  private candidateColumns: string[] = Object.keys(initCandidate);
  public candidates: ICandidate[] = [];
  public filteredCandidates: ICandidate[] = [];
  public filterText = '';
  public text = 'טוען...';

  constructor(private fetcher: FetcherService, private user: UserService) {
  }

  async ngOnInit(): Promise<void> {
    try {
      this.candidates = await this.fetcher.get('getCandidates');
      this.filteredCandidates = this.candidates;
      if (this.candidates.length === 0) {
        this.text = 'רשימת המועמדים ריקה';
      }
    } catch (e) {
      this.text = 'שגיאה בשליפת רשימת המועמדים מהשרת';
    }
  }

  public openQuiz(candidate: ICandidate): void {
    this.user.candidate = candidate;
  }

  public filter(): void {
    if (this.filterText.length > 0) {
      this.filteredCandidates = this.candidates.filter((candidate: ICandidate) => {
        let isFound = false;
        this.candidateColumns.forEach(key => {
          if (String((candidate as any)[key]).includes(this.filterText)) {
            isFound = true;
          }
        });
        return isFound;
      });
    } else {
      this.filteredCandidates = this.candidates;
    }
  }
}
