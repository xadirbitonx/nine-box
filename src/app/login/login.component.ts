import {Component} from '@angular/core';
import {FetcherService} from '../../services/fetcher.service';
import {UserService} from "../../services/user.service";
import {goToHome} from "../../types/types";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent {
  public username = '';
  public password = '';

  constructor(private fetcher: FetcherService, private user: UserService) {
  }

  public async login(): Promise<void> {
    try {
      await this.fetcher.post('login', {username: this.username, password: this.password});
      this.user.isAdmin = true;
      goToHome();
    } catch (e) {
      alert('שם משתמש או ססמא לא נכונים');
    }
  }
}
