export interface ICandidate {
  MA: number | null;
  name: string;
  tafkid: string;
  age: number | null;
  vetek: number | null;
  gender: string;
  darga: string;
  slavesSum: number | null;
  merkaz: string;
  anaf: string;
  mador: string;
}

export interface IQuiz {
  commanderMA: number | null;
  isDirectCommander: boolean;
  MA: number | null;
  bitzua1: number;
  bitzua2: number;
  bitzua3: number;
  potential1: number;
  potential2: number;
  potential3: number;
}

export interface ISotzyometry {
  MA: number;
  potentialScore: number;
  bitzuaScore: number;
}

export interface ICycle {
  _id: string;
  name: string;
}

export const initCandidate: ICandidate = {
  MA: null,
  name: '',
  tafkid: '',
  age: null,
  vetek: null,
  gender: '',
  darga: '',
  slavesSum: null,
  merkaz: '',
  anaf: '',
  mador: ''
};

export interface INineBoxResults {
  bitzua: number;
  potential: number;
}
export interface INineBoxData {
  candidate: ICandidate;
  cycle: ICycle;
  sotzyomeryResult: ISotzyometry | null;
  quizes: IQuiz[];
  results: INineBoxResults | null;
}
export type NineBoxDataDict = { [id: string]: INineBoxData[] };
export const initSotzyometry = {MA: null, potentialScore: null, bitzuaScore: null, cycleId: ''};

export const goToHome = (): void => {
  const button = document.getElementById('home');
  if (button) {
    button.click();
  }
};
